process.env.NODE_ENV = process.env.NODE_ENV || 'development'
process.env.HOST_URL = 'http://127.0.0.1:3000'

const environment = require('./environment')

module.exports = environment.toWebpackConfig()
