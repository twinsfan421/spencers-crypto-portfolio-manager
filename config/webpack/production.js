process.env.NODE_ENV = process.env.NODE_ENV || 'production'
process.env.HOST_URL = 'https://crypto-portfolio-mananger-pro-production.up.railway.app'

const environment = require('./environment')

module.exports = environment.toWebpackConfig()
