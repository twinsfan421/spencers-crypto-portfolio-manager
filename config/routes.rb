Rails.application.routes.draw do

  devise_for :users, path: '', 
    path_names: { 
      sign_in: 'login', 
      sign_out: 'logout',
      edit: 'login_edit'
    }, controllers: {
    omniauth_callbacks: "omniauth_callbacks",
    registrations: "registrations"
  }

  devise_scope :user do
    authenticated :user do
      root :to => 'pages#portfolio'
    end
    unauthenticated :user do
      root to: 'pages#welcome', as: :unauthenticated_root
    end
  end

  resources :users, :only => [:edit, :update]
  resources :currency_purchases, :only => [:index, :edit, :create, :update, :destroy]
  resources :currencies, :only => [:index]
  get 'search', to: 'currencies#search'
  get 'calculate', to: 'currencies#calculate'
  get 'currencies/:slug', to: 'currencies#show'

end
