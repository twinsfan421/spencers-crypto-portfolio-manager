class CurrencyPurchase < ApplicationRecord
    belongs_to :currency
    belongs_to :user

    validates :purchase_price, presence: true, numericality: {greater_than: 0, less_than: 1000000}
    validates :units, presence: true, numericality: {greater_than: 0}
end
