class User < ApplicationRecord
  def to_param
    username
  end
  attr_writer :login

  has_many :currency_purchases
  has_many :currencies, through: :currency_purchases

  devise :database_authenticatable, :registerable, :trackable,
         :recoverable, :rememberable, :validatable, :omniauthable,
         omniauth_providers: %i[twitter], authentication_keys: [:login]

  validates :username, 
            uniqueness: { case_sensitive: false },
            presence: true
  validate :validate_username
  
  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.username = auth.info.nickname
    end
  end

  def login
    @login || self.username || self.email
  end

  def email_required?
    super && provider.blank?
  end

  def password_required?
    super && provider.blank?
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      if conditions[:username].nil?
        where(conditions).first
      else
        where(username: conditions[:username]).first
      end
    end
  end

  def validate_username
    if User.where(email: username).exists?
      errors.add(:username, :invalid)
    end
  end

  def name
    r = ""
    r += self.first_name if self.first_name.present?
    r += " " if r.present?
    r += self.last_name if self.last_name.present?

    if r.present?
      return r
    else
      return self.username
    end
  end

end
