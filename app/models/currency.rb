class Currency < ApplicationRecord
    has_many :currency_purchases
    has_many :users, through: :currency_purchases

    validates :name, presence: true, uniqueness: true
    validates :slug, presence: true, uniqueness: true
    validates :currency_symbol, presence: true
    validates :max_supply, presence: true
    self.per_page = 20
    
    after_commit :flush_cache

    scope :search, -> (term) { where('LOWER(name) LIKE ?', "%#{term}%").to_a }

    def calculate_value(amount)
        price = current_price
        price && amount && (price.to_f * amount.to_f).round(2)
    end

    def self.cached_page(page_number)
        Rails.cache.fetch([name, "page#{page_number}"]) { page(page_number).to_a }
    end

    def flush_cache
        Rails.cache.delete_matched("Currency/*")
    end
    
    def current_price
        latest = CryptoAPI.new(slug: self.slug).latest_slug
        latest && latest.dig("quote", "USD", "price")
    end
end
