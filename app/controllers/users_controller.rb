class UsersController < ApplicationController
    before_action :authenticate_user!

    def edit
        @user = User.find_by_username(params[:id])
        authorize @user
    end

    def update
        @user = User.find_by_username(params[:id])
        authorize @user
        if @user.update_attributes(users_params)
            flash[:notice] = "Profile Updated Successfully."
            redirect_to(edit_user_path(@user))
        else
            render('edit')
        end
    end

    def users_params
      params.require(:user).permit(:first_name, :last_name, :phone)
    end
end