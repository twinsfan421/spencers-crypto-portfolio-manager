class CurrenciesController < ApplicationController
    before_action :authenticate_user!, only: [:index, :show]

    def index
        respond_to do |format|
            format.html
            format.json { render json: CryptoAPI.new(page: params[:page]).latest_page}
        end
    end

    def show
        latest = CryptoAPI.new(slug: params[:slug]).latest_slug
        @currency = latest ? latest : Currency.find_by(slug: params[:slug])
        respond_to do |format|
            format.html
            format.json { render json: @currency }
        end
    end

    def search
        @currencies = Currency.page(params[:page]).search(params[:search].downcase)
        render json: { currencies: @currencies }
    end

    def calculate
        amount = params[:amount]
        render json: {
            currency: currency,
            current_price: currency.current_price,
            amount: amount,
            value: currency.calculate_value(amount)
        }
    end

    private

    def currency
        @currency ||= Currency.find(params[:id])
    end
end
