class PagesController < ApplicationController
  before_action :authenticate_user!, only: [:portfolio]

  def welcome
  end

  def portfolio
    @name = current_user.name
  end
end
