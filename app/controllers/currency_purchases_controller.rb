class CurrencyPurchasesController < ApplicationController
    before_action :authenticate_user!
    before_action :find_currency_purchase_and_authorize, :only => [:edit, :update, :destroy]

    def index
        @currency_purchases = current_user.currency_purchases.joins(:currency).select(:id, :name, :max_supply, :currency_symbol, :slug, :currency_id, :purchase_price, :user_id, :units)
        render json: {
            portfolio: CurrencyPurchasesService.new(currency_purchases: @currency_purchases).with_latest
        }
    end

    def create
        @currency_purchase = CurrencyPurchase.new(currency_purchase_params)
        @currency_purchase.user_id = current_user.id
        authorize @currency_purchase
        if @currency_purchase.save
            cp_with_latest = CurrencyPurchasesService.new(currency_purchase: @currency_purchase).with_latest.first
            render json: {errors: nil, cp: cp_with_latest}
        else
            render json: {errors: @currency_purchase.errors.full_messages, cp: nil}
        end
    end

    def edit
        @currency = @currency_purchase.currency
    end
    
    def update
        if @currency_purchase.update_attributes(currency_purchase_params)
            flash[:notice] = "Cryptocurrency Purchase Updated Successfully."
            redirect_to(root_path)
        else
            @currency = @currency_purchase.currency
            render('edit')
        end
    end

    def destroy
        @currency_purchase.destroy
    end

    private

    def currency_purchase_params
      params.require(:currency_purchase).permit(:units, :purchase_price, :note, :currency_id)
    end

    def find_currency_purchase_and_authorize
        @currency_purchase = CurrencyPurchase.find(params[:id])
        authorize @currency_purchase
    end
end