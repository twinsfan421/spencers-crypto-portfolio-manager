import React from 'react'
import Container from 'react-bootstrap/Container'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import { FaExclamationTriangle } from 'react-icons/fa';

const ResourceNotFound = ({name}) => {
    return (
        <div className="vh-min-body bg-danger">
            <Container className="pt-5">
                <Card className="text-center text-danger">
                <Card.Header>404</Card.Header>
                <Card.Body>
                    <Card.Title>{name} Not Found</Card.Title>
                    <p style={{fontSize: "4.5rem"}}><FaExclamationTriangle /></p>
                    <Card.Text>
                    The {name} you are looking for does not exist.
                    </Card.Text>
                    <Button variant="outline-secondary" href="/currencies">Go to Currencies Page</Button>
                </Card.Body>
                </Card>
            </Container>
        </div>
    )
};

export default ResourceNotFound