import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron'

const HomeJumbotron = () => {
    return (
        <Jumbotron className="bg-primary text-white mb-0" fluid>
            <div className="container">
                <h1 className="display-4">Welcome to Crypto Portfolio Manager!</h1><br/>
                <h4 className="mb-4">Try the calculator below to see how much your Cryptocurrency is worth!</h4>
                <div id="sign-up-button">
                    <span className="mt-3">To save your data and track your Portfolio:</span>
                    <a className="btn btn-secondary btn-lg ml-3" id="sign-up-button" href="/sign_up" role="button">Create an Account!</a>
                </div>
            </div>
        </Jumbotron>
    );
}
export default HomeJumbotron