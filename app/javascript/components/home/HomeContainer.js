import React from 'react';
import CalculatorContainer from '../calculator/CalculatorContainer';
import HomeJumbotron from './HomeJumbotron';

const HomeContainer = () => {
    return (
        <>
            <HomeJumbotron />
            <CalculatorContainer />
        </>
    );
}

export default HomeContainer