import React, { Component } from 'react'
import ax from 'packs/axios'
import DashboardCard from './DashboardCard'
import PortfolioTabs from '../portfolio/PortfolioTabs'
class Dashboard extends Component {
  state = {
    portfolio: null
  }

  componentDidMount() {
    ax.get( `${process.env.HOST_URL}/currency_purchases`)
    .then( (data) => {
      this.setState({
          portfolio: data.data.portfolio
      })
    })
    .catch((error) => {
      console.log(error)
    })
  }

  removeFromPortfolio = (index) => {
    const data = this.state.portfolio;
    this.setState({ 
      portfolio: [...data.slice(0,index), ...data.slice(index+1)]
    });
  }

  addToPortfolio = (portfolio) => {
    this.setState({
      portfolio: [...this.state.portfolio, portfolio]
    })
  }

  render(){
    return(
      <div className="bg-primary vh-min-body">
        <div className="container pt-4">
          <DashboardCard user={this.props.user} portfolio={this.state.portfolio} />
          <PortfolioTabs portfolio={this.state.portfolio} addToPortfolio={this.addToPortfolio} removeFromPortfolio={this.removeFromPortfolio}/>
        </div>
      </div>
    )
}

}

export default Dashboard