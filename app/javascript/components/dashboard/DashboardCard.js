import React from 'react';
import Container from 'react-bootstrap/Container'
import Card from 'react-bootstrap/Card'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import DashboardList from './DashboardList';
import DashboardChart from './DashboardChart'
import ProgressBar from 'react-bootstrap/ProgressBar';

const DashboardCard = ({user, portfolio}) => {
    return (
        <Card className="mb-4">
            <Card.Header as="h4" className="bg-light text-primary">{user.name}</Card.Header>
            <Card.Body>
                <Card.Title as="h3" className="text-center text-secondary mb-3">Portfolio Overview</Card.Title>
                <Container>
                    <Row>
                        <Col md={5}>
                            <DashboardList portfolio={portfolio} />
                        </Col>
                        <Col md={7}>
                            {portfolio ? <DashboardChart portfolio={portfolio} /> : <ProgressBar animated now={45} /> }
                        </Col>
                    </Row>
                </Container>
            </Card.Body>
        </Card>
    );
}
export default DashboardCard