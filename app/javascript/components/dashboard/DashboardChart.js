import React from 'react'
import Card from 'react-bootstrap/Card'
import Chart from "react-apexcharts";

const DashboardChart = ({portfolio}) => {
    const data = portfolio.map(p => p.total.current ? p.total.current : 0)
    const names = portfolio.map(p => p.data.name)
    const series = [...data]
    const options = {
        chart: {
          height: 250,
          type: 'pie',
        },
        labels: [...names],
          responsive: [{
          breakpoint: 480,
          options: {
            chart: {
            width: 150
          },
            legend: {
            position: 'bottom'
          }
        }
        }]
      }   

    return(
        <Card className="bg-light text-primary">
            <Card.Body>
                <Card.Title className="text-center">Allocation Summary</Card.Title>
                    <Chart
                    options={options} 
                    series={series}
                    type="pie" 
                    height={200} 
                    />
            </Card.Body>
        </Card>
    )
}

export default DashboardChart