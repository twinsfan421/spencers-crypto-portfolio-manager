import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup';

const DashboardList = ({portfolio}) => {
    var currentTotal = 0
    var startedTotal = 0
    var count = portfolio ? portfolio.length : 0
    portfolio && portfolio.map( p => {
        startedTotal += p.total.started
        if (p.total.current) currentTotal += p.total.current
    })
    const change = currentTotal - startedTotal
    return(
        <ListGroup>
            <ListGroup.Item><div className="text-primary">Purchased At Price: </div>${startedTotal.toFixed(2)}</ListGroup.Item>
            <ListGroup.Item><div className="text-primary">Portfolio Current Value: </div>${currentTotal.toFixed(2)}</ListGroup.Item>
            <ListGroup.Item><div className="text-primary">+/- </div> <div style={{color: change < 0 ? 'red' : 'green'}}>{change.toFixed(2)}</div></ListGroup.Item>
            <ListGroup.Item><div className="text-primary">Portfolio count: </div>{count}</ListGroup.Item>
        </ListGroup>
    )
}

export default DashboardList