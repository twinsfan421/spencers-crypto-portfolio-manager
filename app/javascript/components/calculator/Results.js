import React from 'react'
import ResultsTableItem from "./ResultsTableItem"
import Table from 'react-bootstrap/Table'

const Results = ({portfolioItems}) => {

    var amount = 0
    const resultsTableItems = portfolioItems.map( (item, index) => {
        if (item.value) amount += item.value
        let n = index + 1
        return <ResultsTableItem item={item} key={index} n={n} />})
    return(
        <div className="pt-4">
            <div>
                <h2 className="ml-2 mb-4">Your Total Portfolio Value Is: 
                ${(amount).toFixed(2).toLocaleString('en-US', { style: 'currency', currency: 'USD' })}</h2>
            </div>
            <Table striped bordered hover responsive variant="secondary" id="results-table">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Currency</th>
                    <th>Current Price</th>
                    <th>Unit Count</th>
                    <th>Current Value</th>
                    </tr>
                </thead>
                <tbody>
                    {resultsTableItems}
                </tbody>
            </Table>
        </div>
    )
}

export default Results