import React from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

const Calculate = ({handleAmount, active_currency, handleSubmit}) => {

    return(
        <div className="pt-4">
            <h1>Hom much {active_currency.name} do you own?</h1>
            <Form onSubmit={handleSubmit}>
                <Form.Group controlId="formBasicAmount">
                    <Form.Label>Enter amount owned</Form.Label>
                    <Form.Control type="number" onChange={handleAmount} autoComplete="off" placeholder="How much do you own?" required/>
                </Form.Group>
                <Button size="lg" variant="secondary" type="submit">Calculate Total Value</Button>
            </Form>
        </div>        
    )
}

export default Calculate