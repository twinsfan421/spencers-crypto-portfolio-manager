import React, { Component } from 'react'
import Search from '../search/Search'
import Calculate from './Calculate'
import Results from './Results'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import ax from 'packs/axios'

class CalculatorContainer extends Component {

  state = {
    portfolio: [],
    active_currency: null,
    amount: ''
  }

  handleSubmit = (e) => {
    e.preventDefault()
    let currency = this.state.active_currency.id
    let amount = this.state.amount

    ax.get(`${process.env.HOST_URL}/calculate`, {
        params: {
        id: currency,
        amount: amount
      }
    })
    .then( (data) => {
      this.setState({
          amount: '',
          active_currency: null,
          portfolio: [...this.state.portfolio, data.data]
      })
    })
    .catch( (error) => {
      console.log(error)
    })
  }

  handleSelect = (currency) => {
    this.setState({
      active_currency: currency
    })
  }

  handleAmount = (e) =>{
    this.setState({
      amount: e.target.value
    })
  }

  render(){
    return(
      <Container className="m-0" fluid>
        <Row className="min-calculator-height">
          <Col md={6} className="bg-light text-dark">
            <h1>Cryptocurrency Calculator</h1>
            {this.state.active_currency ? 
            <Calculate 
            handleSubmit={this.handleSubmit}
            handleAmount={this.handleAmount}
            active_currency={this.state.active_currency}
            amount={this.state.amount}
            /> : 
            <Search handleSelect={this.handleSelect} />
           }
          </Col>
          <Col md={6} className="bg-dark text-light">
            <Results portfolioItems={this.state.portfolio}/>
          </Col>
        </Row>
      </Container>
    )
}

}

export default CalculatorContainer