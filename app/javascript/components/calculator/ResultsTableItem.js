import React from 'react'

const ResultsTableItem = ({item, n}) => {
    return(
        <tr>
            <td>{n}</td>
            <td>{item.currency.name}</td>
            <td>{item.current_price ? item.current_price : "N/A"}</td>
            <td>{item.amount}</td>
            <td>${item.value ? item.value : "N/A"}</td>
        </tr>
    )
}

export default ResultsTableItem