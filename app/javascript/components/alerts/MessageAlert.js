import React from 'react';
import Alert from 'react-bootstrap/Alert'

const MessageAlert = ({heading, message, variant}) => {
    return (
        <Alert variant={variant}>
            <Alert.Heading>{heading}</Alert.Heading>
            {message}
        </Alert>
    );
}
export default MessageAlert