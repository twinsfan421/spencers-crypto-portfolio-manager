import React from 'react';
import Alert from 'react-bootstrap/Alert'

const FormErrorAlert = ({messages}) => {
    return (
        <Alert variant="danger">
            <Alert.Heading>Please fix the following form errors</Alert.Heading>
            {messages}
        </Alert>
    );
}
export default FormErrorAlert