import React, { useState } from 'react';
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import Card from 'react-bootstrap/Card'
import Currencies from './Currencies';
import Container from 'react-bootstrap/Container'
import CurrencySearch from './CurrencySearch';

const CurrenciesTabs = () => {
    const [key, setKey] = useState('currencies');
    return(
        <div className="bg-secondary py-4 vh-min-body">
            <Container>
                <Card bg="light" className="min-height-30">
                    <Card.Body>
                        <Tabs id="currency-tabs" unmountOnExit={true} activeKey={key} onSelect={k => setKey(k) }>
                            <Tab eventKey="currencies" title="Table">
                                <Currencies />
                            </Tab>
                            <Tab eventKey="search" title="Search">
                                <CurrencySearch />
                            </Tab>
                        </Tabs>
                    </Card.Body>
                </Card>
            </Container>
        </div>
    )
}

export default CurrenciesTabs