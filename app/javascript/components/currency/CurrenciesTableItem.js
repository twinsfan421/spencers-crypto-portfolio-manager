import React from 'react'
import { FaChartLine } from 'react-icons/fa';

const CurrenciesTableItem = ({item, handleChartShow}) => {
    const currency = item
    const latest = item.quote.USD
    const change24 = latest && latest.percent_change_24h ? latest.percent_change_24h.toFixed(2) : false
    const volume = latest && latest.volume_24h ? Math.round(latest.volume_24h).toLocaleString('en-US', { style: 'currency', currency: 'USD' }) : "N/A"
    const price = latest && latest.price ? latest.price.toFixed(6).toLocaleString('en-US', { style: 'currency', currency: 'USD' }) : "N/A"
    return(
        <tr>
            <td>{currency.cmc_rank}</td>
            <td><a href={`/currencies/${item.slug}`}>{item.symbol}</a></td>
            <td><a href={`/currencies/${item.slug}`}>{item.name}</a></td>
            <td>{volume}</td>
            <td>${price}</td>
            <td style={{color: change24 <= 0 ? 'red' : 'green'}}>{change24 ? change24 : "N/A"}%</td>
            <td>{latest ? <FaChartLine onClick={() => handleChartShow(item)} /> : "N/A"}</td>
        </tr>
    )
}

export default CurrenciesTableItem