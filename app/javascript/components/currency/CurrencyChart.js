import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Chart from "react-apexcharts";
import moment from 'moment'
import ListGroup from 'react-bootstrap/ListGroup'

const CurrencyChart = ({currency}) => {
    const latest = currency.quote ? currency.quote.USD : currency.latest
    const data = currency.data ? currency.data : currency
    const price = latest.price
    const applyPercent = (price, percent) => { return (price * (1 + percent / 100)).toFixed(2) }
    const series = [
        {
          name: data.name,
          data: [latest.price, applyPercent(price, latest.percent_change_1h), applyPercent(price, latest.percent_change_24h), applyPercent(price, latest.percent_change_7d)]
        }
      ]
    const options = {
        chart: {
          height: 350,
          type: 'line',
          zoom: {
            enabled: false
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: 'straight'
        },
        title: {
          text: 'Latest Pricing Information',
          align: 'center'
        },
        grid: {
          row: {
            colors: ['#f3f3f3', 'transparent'],
            opacity: 0.5
          },
        },
        xaxis: {
          categories: ['Current', '1 Hour ago', '1 Day ago', '1 Week ago'],
        }
      }   
    return(
      <Container className="m-0" fluid>
        <Row>
          <Col lg="3" className="pt-5">
            <ListGroup>
              <ListGroup.Item><div className="text-primary">{data.name} {data.currency_symbol}</div></ListGroup.Item>
              <ListGroup.Item><div className="text-primary">Max Supply: </div>{data.max_supply}</ListGroup.Item>
              <ListGroup.Item><div className="text-primary">24 Hour Volume: </div> {latest.volume_24h}</ListGroup.Item>
              <ListGroup.Item><div className="text-primary">Last Updated: </div> {moment(latest.last_updated).calendar()}</ListGroup.Item>
            </ListGroup>
          </Col>
          <Col lg="9">
              <Chart
              options={options} 
              series={series}
              type="line" 
              height={350} 
              />
          </Col>
        </Row>
      </Container>
    )
}

export default CurrencyChart