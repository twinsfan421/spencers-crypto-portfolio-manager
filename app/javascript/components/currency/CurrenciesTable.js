import React from 'react';
import CurrenciesTableItem from './CurrenciesTableItem'
import Table from 'react-bootstrap/Table'
import { FaChartArea } from 'react-icons/fa';

const CurrenciesTable = ({currencies, handleChartShow}) => {
    const currenciesTableItems = currencies && currencies.map( (item, index) => {
        let n = index + 1
        return <CurrenciesTableItem item={item} key={index} n={n} handleChartShow={handleChartShow}/>})
    return(
        <Table striped bordered hover responsive variant="light" id="currencies-table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Symbol</th>
                    <th>Name</th>
                    <th>24h Volume</th>
                    <th>Current Price</th>
                    <th>+/- 24h</th>
                    <th>Chart <FaChartArea/></th>
                </tr>
            </thead>
            <tbody>
                {currenciesTableItems}
            </tbody>
        </Table>
    )
}

export default CurrenciesTable