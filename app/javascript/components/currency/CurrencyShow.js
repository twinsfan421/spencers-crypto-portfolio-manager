import React from 'react'
import CurrencyChart from './CurrencyChart'
import CurrencyCard from './CurrencyCard'
import ResourceNotFound from '../error/ResourceNotFound'

const CurrencyShow = ({currency}) => {
    const latest = currency && currency.quote && currency.quote.USD
    if (!currency) {
        return <ResourceNotFound name={"Currency"} />
    }
    return(
        <>
            <h2 className="text-center">{currency.name}</h2>
            <h5 className="text-center py-2 text-primary">Currenct Price: ${latest ? latest.price : "N/A"}</h5>
            {latest ? <CurrencyChart currency={currency}/> : <CurrencyCard currency={currency}/>}
        </>
    )
}

export default CurrencyShow