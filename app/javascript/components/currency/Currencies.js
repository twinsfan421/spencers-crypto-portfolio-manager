import React, { Component } from 'react'
import ax from 'packs/axios'
import ChartModal from '../modals/ChartModal';
import CurrencyChart from './CurrencyChart';
import Button from 'react-bootstrap/Button'
import ProgressBar from 'react-bootstrap/ProgressBar';
import CurrenciesTable from './CurrenciesTable';
import moment from 'moment'
import MessageAlert from '../alerts/MessageAlert';

class Currencies extends Component {

  state = {
    currencies: null,
    page: 1,
    show: false, 
    currency: null,
    loading: false,
    error: null
  }

  getLatestCurrency = () => {
    this.setState({loading: true})
    ax.get(`${process.env.HOST_URL}/currencies`, {
        headers: {accept: 'application/json'},
        params: {page: this.state.page}
    })
    .then( (response) => {
        console.log(response)
        if (response.data) {
        this.setState({
          currencies: response.data,
          loading: false
        })
        }else {
          this.setState({
            currencies: [],
            loading: false,
            error: "Latest cryptocurrency data not available. Try again later"
          })
        }
    })
    .catch( (error) => {
        this.setState({loading: false})
        console.log(error)
    })
  }
  handleChartClose = () => {this.setState({show: false})};
  handleChartShow = (currency) => {this.setState({show: true, currency: currency})};
  nextPage = () => {
    this.setState(prevState => {
       return {page: prevState.page + 1}
    }, () => {this.getLatestCurrency()})
  }
  previousPage = () => {
    this.setState(prevState => {
       return {page: prevState.page - 1}
    }, () => {this.getLatestCurrency()})
  }
  componentDidMount() {
    this.getLatestCurrency()
  }
  render(){
    const isLoading = this.state.loading
    const currencies = this.state.currencies
    const first = currencies && currencies[0]
    return(
      <>
          <h2 className="py-3 text-center text-primary">Cryptocurrencies</h2>
          { this.state.error && <MessageAlert message={this.state.error} heading={""} variant={"danger"} /> }
          <h6 className="text-secondary">Last Updated: {first && moment(first.quote.USD.last_updated).calendar()}</h6>
          <CurrenciesTable currencies={currencies} handleChartShow={this.handleChartShow} />
          { !currencies && <ProgressBar animated now={45} /> }
          <ChartModal show={this.state.show} handleChartClose={this.handleChartClose}>
            <CurrencyChart currency={this.state.currency} />
          </ChartModal>
          {this.state.page !== 1  && 
            <Button variant="secondary" className="float-left" onClick={() => {!isLoading ? this.previousPage() : null}} disabled={isLoading}>Previous 100</Button>}
          {currencies && currencies.length === 100 &&
            <Button variant="secondary" className="float-right" onClick={() => {!isLoading ? this.nextPage() : null}} disabled={isLoading}>Next 100</Button>}
      </>
    )
  }

}

export default Currencies