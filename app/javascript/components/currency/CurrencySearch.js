import React, { Component } from 'react'
import Search from '../search/Search'
import ax from 'packs/axios'
import Container from 'react-bootstrap/Container'
import ProgressBar from 'react-bootstrap/ProgressBar';
import CurrencyShow from './CurrencyShow'
import Button from 'react-bootstrap/Button'

class CurrencySearch extends Component {

  state = {
    active_currency: null,
    loading: false,
    activeLatest: null,
    message: null
  }

  getLatestCurrency = (slug) => {
    ax.get(`${process.env.HOST_URL}/currencies/${slug}`, {
      headers: {accept: 'application/json'}
    })
    .then( (response) => {
        this.setState({
          activeLatest: response.data,
          loading: false
        })
    })
    .catch( (error) => {
      console.log(error)
      this.setState({
        loading: false
      })
    })
  }

  handleSelect = (currency) => {
    let active = currency
    this.setState({
      active_currency: active,
      loading: true
    })
    this.getLatestCurrency(active.slug)
  }

  clearActiveLatest = () => {
      this.setState({
          activeLatest: null,
          active_currency: null
    })
  }

  render(){
    const active = this.state.activeLatest ? <CurrencyShow currency={this.state.activeLatest} /> : <ProgressBar animated now={45} />
    return(
      <Container className="m-0 min-height-30">
        <h3 className="text-center my-3">Cryptocurrency Search</h3>
        { this.state.activeLatest && <Button variant="primary" size="lg" onClick={this.clearActiveLatest}>Back to Search</Button> }
            { this.state.active_currency ?
              active :
              <Search handleSelect={this.handleSelect} />
            }
      </Container>
    )
  }

}

export default CurrencySearch
