import React from 'react';
import Card from 'react-bootstrap/Card'
import ListGroup from 'react-bootstrap/ListGroup'

const CurrencyCard = ({currency}) => {
    return (
        <Card bg="secondary">
            <Card.Body>
                <Card.Title as="h5" className="text-center text-light mb-4">{currency.name}</Card.Title>
                <ListGroup>
                    <ListGroup.Item><div className="text-primary">Currency Symbol: </div>{currency.currency_symbol}</ListGroup.Item>
                    <ListGroup.Item><div className="text-primary">Slug: </div> {currency.slug}</ListGroup.Item>
                    <ListGroup.Item><div className="text-primary">Max Supply: </div> {currency.max_supply}</ListGroup.Item>
                    <ListGroup.Item><div className="text-primary">Coin Market Cap Id: </div>{currency.cmc_id}</ListGroup.Item>
                </ListGroup>
            </Card.Body>
        </Card>
    );
}
export default CurrencyCard