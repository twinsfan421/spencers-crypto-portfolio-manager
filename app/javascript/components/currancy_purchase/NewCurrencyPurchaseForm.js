import React from 'react'
import Form from 'react-bootstrap/Form'
import FormErrorAlert from '../alerts/FormErrorAlert'
import Button from 'react-bootstrap/Button'

const NewCurrencyPurchaseForm = ({errors, handleSubmit, handleFormChange, cp, disabled}) => {
    const messages = errors.map( (error, index) => {
    return <p key={index}>{error}</p>})
    return (
        <>
            { messages.length ?  <FormErrorAlert messages={messages} /> : null}
            <Form onSubmit={handleSubmit} className="pt-4">
                <Form.Group controlId="purchase_price">
                    <Form.Label>Purchase Price</Form.Label>
                    <Form.Control type="number" onChange={handleFormChange} value={cp.currency_purchase || undefined} placeholder="what price did you purchase your cryptocurrancy at" min={0} step="any" required/>
                </Form.Group>
                <Form.Group controlId="units">
                    <Form.Label>Units</Form.Label>
                    <Form.Control type="number" onChange={handleFormChange} value={cp.units || 0} placeholder="how much did you purchase" min={1} required/>
                </Form.Group>
                <Form.Group controlId="note">
                    <Form.Label>Note</Form.Label>
                    <Form.Control onChange={handleFormChange} value={cp.note || ''} placeholder="add a note about your purchase" />
                </Form.Group>
                <Button variant="primary" type="submit" disabled={disabled} block>
                  Create Cryptocurrancy Purchase
                </Button>
                {disabled && <p className="text-danger">select a cryptocurrancy</p>}
            </Form>
        </>
    )
}
export default NewCurrencyPurchaseForm