import React, { Component } from 'react'
import Search from '../search/Search'
import ax from 'packs/axios'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import NewCurrencyPurchaseForm from './NewCurrencyPurchaseForm'
import CurrencyCard from '../currency/CurrencyCard'
import MessageAlert from '../alerts/MessageAlert'

class NewCurrencyPurchase extends Component {

  state = {
    cp: {
      units: null,
      purchase_price: null,
      note: null,
      currency_id: null
    },
    active_currency: null,
    errors: [],
    message: null
  }

  handleSubmit = (e) => {
    e.preventDefault()
    let cp = this.state.cp
    cp.currency_id = this.state.active_currency.id

    ax.post(`${process.env.HOST_URL}/currency_purchases`, {
      currency_purchase: cp
    })
    .then( (response) => {
      if (!response.data.errors) {
          this.props.addToPortfolio(response.data.cp),
          this.setState({
            message: "New Currency Purchase Created!",
            active_currency: null,
            search_results: [],
            errors: [],
            cp: {
              units: null,
              purchase_price: null,
              note: null,
              currency_id: null
            }
          })
      } else {
        this.setState({
          errors: response.data.errors
        })
      }
    })
    .catch( (error) => {
      console.log(error)
    })
  }

  handleSelect = (currency) => {
    this.setState({
      active_currency: currency
    })
  }

  parseTargetValue = (target) => {
    let value
    switch (target.id) {
      case "note":
        value = target.value;
        break;
      case "purchase_price":
        value = parseFloat(target.value);
        break;
      case "units":
        value = parseInt(target.value, 10);
        break;
      default:
        value = target.value;
    }
    return value
  }

  handleFormChange = (e) => {
    let target = e.target
    let value = this.parseTargetValue(target)
    this.setState({
        cp: { ...this.state.cp, [e.target.id]: value }
    })
  }

  render(){
    let disabled = this.state.active_currency ? false : true
    return(
      <Container className="m-0 min-height-30" fluid>
        <h3 className="text-secondary text-center my-3">New Cryptocurrency Purchase</h3>
        {this.state.message && <MessageAlert message={this.state.message} heading={"Success!"} variant={"success"} />}
        <Row>
          <Col md={6}>
            { !disabled ? 
              <CurrencyCard currency={this.state.active_currency} /> :
              <Search handleSelect={this.handleSelect} />
            }
          </Col>
          <Col md={6}>
            <NewCurrencyPurchaseForm cp={this.state.cp} errors={this.state.errors} handleFormChange={this.handleFormChange} handleSubmit={this.handleSubmit} disabled={disabled} />
          </Col>
        </Row>
      </Container>
    )
  }

}

export default NewCurrencyPurchase