import React from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'

const DeleteModal = (props) => {
    return (
        <Modal show={props.show} onHide={props.handleClose} centered>
            <Modal.Header closeButton>
            <Modal.Title>Delete {props.title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>Are you sure you want to Delete?</Modal.Body>
            <Modal.Footer>
            <Button variant="secondary" onClick={props.handleClose}>
                Cancel
            </Button>
            <Button variant="primary" onClick={props.handleDelete}>
                Confirm Delete
            </Button>
            </Modal.Footer>
        </Modal>
    )
}
export default DeleteModal