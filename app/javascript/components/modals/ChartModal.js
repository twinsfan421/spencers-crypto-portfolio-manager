import React from 'react'
import Modal from 'react-bootstrap/Modal'

const ChartModal = ({show, handleChartClose, children}) => {
    return (
        <Modal 
            show={show}
            centered
            size="lg"
            onHide={() => handleChartClose()}
            >
            <Modal.Header closeButton>
            </Modal.Header>
            <Modal.Body>
                {children}
            </Modal.Body>
        </Modal>
    )
}
export default ChartModal