import React, { useState } from 'react';
import Portfolio from './Portfolio'
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import NewCurrencyPurchase from '../currancy_purchase/NewCurrencyPurchase'
import Card from 'react-bootstrap/Card'
import ProgressBar from 'react-bootstrap/ProgressBar'

const PortfolioTabs = ({portfolio, removeFromPortfolio, addToPortfolio}) => {
    const [key, setKey] = useState('profile');
    return(
        <Card bg="light min-height-30">
            <Card.Body>
                <Tabs id="dashboard-tabs" unmountOnExit={true} activeKey={key} onSelect={k => setKey(k) }>
                    <Tab eventKey="profile" title="My Cryptocurrencies">
                    <h3 className="text-secondary text-center py-3">Cryptocurrencies Overview</h3>
                        { portfolio ? <Portfolio portfolio={portfolio} removeFromPortfolio={removeFromPortfolio} /> :
                            <ProgressBar animated now={45} />
                        }
                    </Tab>
                    <Tab eventKey="new" title="New Cryptocurrency">
                        <NewCurrencyPurchase addToPortfolio={addToPortfolio}/>
                    </Tab>
                </Tabs>
            </Card.Body>
        </Card>
    )
}

export default PortfolioTabs