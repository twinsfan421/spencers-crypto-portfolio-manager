import React from 'react'
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'
import { FaChartLine } from 'react-icons/fa';

const PortfolioTableItem = ({item, n, handleShow, handleChartShow}) => {
    const latest = item.latest
    const data = item.data
    const total = item.total.current ? item.total.current.toFixed(2) : "N/A"
    const value = latest ? (latest.price).toFixed(2) : "N/A"
    const adjusted = latest ? (latest.price - data.purchase_price).toFixed(2) : "N/A"
    return(
        <tr>
            <td>{n}</td>
            <td><a href={`/currencies/${data.slug}`}>{data.name}</a></td>
            <td>{data.units}</td>
            <td>${parseFloat(data.purchase_price).toFixed(2)}</td>
            <td>${value}</td>
            <td style={{color: adjusted < 0 ? 'red' : 'green'}}>{adjusted}</td>
            <td>${total}</td>
            <td className="chart">{ latest ? <FaChartLine onClick={() => handleChartShow(item)}/> : "N/A"}</td>
            <td>
                <DropdownButton id="dropdown-basic-button" title="">
                    <Dropdown.Item href={`/currency_purchases/${data.id}/edit`}>Edit</Dropdown.Item>
                    <Dropdown.Item onClick={() => handleShow({index: n - 1, data: item.data})} as={"button"}>Delete</Dropdown.Item>
                </DropdownButton>
            </td>
        </tr>
    )
}

export default PortfolioTableItem