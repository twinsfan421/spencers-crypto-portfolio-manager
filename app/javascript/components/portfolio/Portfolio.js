import React, { useState } from 'react';
import ax from 'packs/axios'
import DeleteModal from '../modals/DeleteModal';
import ChartModal from '../modals/ChartModal';
import CurrencyChart from '../currency/CurrencyChart';
import PortfolioTable from './PortfolioTable';

const portfolio = ({portfolio, removeFromPortfolio}) => {
    const [deleteCP, setDeleteCP] = useState({show: false, cp: null});
    const [chart, setChart] = useState({show: false, cp: null});
    const handleClose = () => setDeleteCP({show: false, cp: null});
    const handleShow = (cp) => {setDeleteCP({show: true, cp: cp})};
    const handleChartClose = () => {setChart({show: false, cp: chart.cp})};
    const handleChartShow = (cp) => {setChart({show: true, cp: cp})};
    const handleDelete = () => {
        ax.delete( `${process.env.HOST_URL}/currency_purchases/${deleteCP.cp.data.id}`)
        .then(() => {
          removeFromPortfolio(deleteCP.cp.index)
          handleClose()
        })
        .catch((error) => {
          console.log(error)
        })
    };
    return(
        <>
            <PortfolioTable portfolio={portfolio} handleChartShow={handleChartShow} handleShow={handleShow}/>
            <ChartModal show={chart.show} handleChartClose={handleChartClose}>
                <CurrencyChart currency={chart.cp} />
            </ChartModal>
            <DeleteModal show={deleteCP.show} handleClose={handleClose} handleDelete={handleDelete} title={"Cryptocurrency Purchase"}/>
        </>
    )
}

export default portfolio