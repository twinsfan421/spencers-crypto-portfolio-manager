import React from 'react';
import Table from 'react-bootstrap/Table'
import { FaChartArea } from 'react-icons/fa';
import PortfolioTableItem from './PortfolioTableItem';

const PortfolioTable = ({portfolio, handleChartShow, handleShow}) => {
    const portfolioItems = portfolio && portfolio.map((item, index) => {
        let n = index + 1
        return <PortfolioTableItem item={item} n={n} key={n} handleShow={handleShow} handleChartShow={handleChartShow}/>})
    return(
        <Table bg="light" striped hover responsive className="text-center">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Units</th>
                    <th>Purchase Price</th>
                    <th>Current Value</th>
                    <th>+/-</th>
                    <th>Total Value</th>
                    <th>Chart <FaChartArea/></th>
                    <th>Modify</th>
                </tr>
            </thead>
            <tbody>
                {portfolioItems}
            </tbody>
        </Table>
    )
}

export default PortfolioTable