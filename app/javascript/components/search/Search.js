import React, { useState } from 'react'
import InfiniteScroll from 'react-infinite-scroller';
import ax from 'packs/axios'
import ProgressBar from 'react-bootstrap/ProgressBar';
import SearchList from './SearchList';
import SearchForm from './SearchForm';
import Badge from 'react-bootstrap/Badge'
import { FaWindowClose } from 'react-icons/fa';

const Search = ({handleSelect}) => {
    const [search, setSearch] = useState({page: 1, results: [], more: false, term: "", selected: null});
    const handleChange = (e) => {
        let value = e.target.value
        let more
        ax.get( `${process.env.HOST_URL}/search`, {
            params: {search: value, page: search.page}
          })
          .then((data) => {
              data.data.currencies.length < 10 ? (more = false) : (more = true)
              setSearch({page: 1, results: [...data.data.currencies], more: more, term: value, selected: null})
          })
          .catch((error) => {
            console.log(error)
          })
    }

    const selectSearch = (value) => { 
        const activeCurrency = search.results.filter( item => item.id == parseInt(value))
        handleSelect(activeCurrency[0])
    }

    const getMoreCurrencies = () => {
        let page = search.page + 1
        let name = search.selected ? search.selected : search.term
        let more
        ax.get( `${process.env.HOST_URL}/search`, {
          params: {search: name, page: page}
        })
        .then( (data) => {
            data.data.currencies.length < 10 ? (more = false) : (more = true)
            setSearch({...search, page: page, results: [...search.results, ...data.data.currencies], more: more})
        })
        .catch((error) => {
          console.log(error)
        })
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        setSearch({...search, selected: search.term, term: ""})
    }

    return(
    <>
        <h4 className="text-secondary text-center mb-3">Select a Cryptocurrency</h4>
        <SearchForm name={"Currency"} handleChange={handleChange} handleSubmit={handleSubmit} value={search.term} />
        { search.selected &&
            <h4>
                Searching: <Badge pill variant="primary">{search.selected} <FaWindowClose onClick={() => {setSearch({page: 1, results: [], more: false, term: "", selected: null})}}/></Badge>
            </h4>
        }
        <InfiniteScroll
            pageStart={0}
            loadMore={getMoreCurrencies}
            hasMore={search.more}
            loader={<div className="loader" key={0}><ProgressBar animated now={45} /></div>}
            >
            <div className="section">
            <SearchList
                selectSearch={selectSearch}
                searchResults={search.results}
                />
            </div>
        </InfiniteScroll>
    </>
    )
}

export default Search