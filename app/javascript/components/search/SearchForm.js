import React from 'react'
import Form from 'react-bootstrap/Form'

const SearchForm = ({name, handleChange, handleSubmit, value}) => {
    return(
        <Form className="pt-4" onSubmit={handleSubmit}>
            <Form.Group controlId="formBasicName">
                <Form.Label as="h3">Search for a {name}:</Form.Label>
                <Form.Control type="name" value={value} onChange={handleChange} autoComplete="off" placeholder="Example: Bitcoin, litecoin..." />
            </Form.Group>
        </Form>
    )
}

export default SearchForm