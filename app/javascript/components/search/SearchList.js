import React from 'react'
import ListGroup from 'react-bootstrap/ListGroup'

const SearchList = ({selectSearch, searchResults}) => {
        const searchList = searchResults.map( curr => <ListGroup.Item as="li" variant="secondary" key={curr.id} action onClick={(e) => selectSearch(e.currentTarget.value)} value={curr.id}>
            <span>{curr.name}</span><span className="float-right">{curr.currency_symbol}</span></ListGroup.Item>)
        return(
            <ListGroup id="search-list" as="ul">
                {searchList}
            </ListGroup>
        )
}

export default SearchList