import React from 'react'
import ReactDOM from 'react-dom'
import CurrencyShow from '../components/currency/CurrencyShow'

document.addEventListener('DOMContentLoaded', () => {
  const node = document.getElementById('currency_data')
  const data = JSON.parse(node.getAttribute('data'))
  ReactDOM.render(
    <CurrencyShow currency={data} />,
    document.body.appendChild(document.createElement('div')),
  )
})