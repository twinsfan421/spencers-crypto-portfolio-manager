import React from 'react'
import ReactDOM from 'react-dom'
import Dashboard from '../components/dashboard/Dashboard'

document.addEventListener('DOMContentLoaded', () => {
  const node = document.getElementById('user_data')
  const data = JSON.parse(node.getAttribute('data'))
  ReactDOM.render(
    <Dashboard user={data}/>,
    document.body.appendChild(document.createElement('div')),
  )
})