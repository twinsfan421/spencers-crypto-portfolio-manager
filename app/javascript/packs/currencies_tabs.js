import React from 'react'
import ReactDOM from 'react-dom'
import CurrenciesTabs from '../components/currency/CurrenciesTabs'

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <CurrenciesTabs />,
    document.body.appendChild(document.createElement('div')),
  )
})