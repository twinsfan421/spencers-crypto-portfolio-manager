import React from 'react'
import ReactDOM from 'react-dom'
import HomeContainer from '../components/home/HomeContainer'


document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <HomeContainer />,
    document.body.appendChild(document.createElement('div')),
  )
})
