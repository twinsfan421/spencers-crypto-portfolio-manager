class UserPolicy < ApplicationPolicy

    def edit?
        current_user?
    end

    def update?
        current_user?
    end

    def current_user?
        user == record
    end
end