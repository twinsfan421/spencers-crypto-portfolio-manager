class CurrencyPurchasePolicy < ApplicationPolicy

    def create?
        user_is_authorized?
    end

    def edit?
        user_is_authorized?
    end

    def update?
        user_is_authorized?
    end

    def destroy?
        user_is_authorized?
    end

    def user_is_authorized?
        user.id == record.user_id
    end
end