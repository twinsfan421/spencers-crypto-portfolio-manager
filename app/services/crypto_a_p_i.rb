class CryptoAPI
    def initialize(page: 1, slug: nil, currencies: nil)
        @page = page
        @slug = slug
        @currencies = currencies
    end

    def cached_latest
        latest = Rails.cache.fetch('latest', expires_in: 45.minutes) { CoinMarketCap.new(query: { 'limit' => '5000' }).listings_latest }
    end

    def cached_data
        latest = cached_latest && cached_latest.fetch('data')
    end

    def latest_page
        latest = cached_data
        return nil if !latest
        paginated = latest.paginate(:page => @page, :per_page => 100)
    end

    def latest_slug(slug: @slug)
        latest = cached_data
        return nil if !latest
        latest.each do |l|
            return l if (l.fetch("slug") == slug)
        end
        nil
    end

    def latest_currencies
        new_c = []
        @currencies.each do |c|
            latest = latest_slug(slug: c.fetch("slug"))
            new_c.push(latest)
        end
        new_c
    end
end