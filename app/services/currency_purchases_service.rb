class CurrencyPurchasesService
    def initialize(currency_purchase: nil, currency_purchases: nil)
        @currency_purchase = currency_purchase
        @currency_purchases = currency_purchases
    end

    def with_latest
        cp =  (@currency_purchases.as_json) || currency_purchase
        populate_with_latest(cp)
    end
  
    private

    def currency_purchase
        currency = @currency_purchase.currency.as_json
        [currency.merge(@currency_purchase.as_json)]
    end

    def populate_with_latest(cp)
        new_cp = []
        cp.each do |c|
            latest = CryptoAPI.new(slug: c.fetch("slug")).latest_slug
            usd = latest ? latest.dig("quote", "USD") : nil
            price = usd ? usd.fetch("price") : nil
            started = c.fetch("units") * c.fetch("purchase_price").to_f
            current = price ? c.fetch("units") * price : price
            new_cp.push({data: c, latest: usd, total: {started: started.to_f, current: current.to_f}})
        end
        new_cp
    end
end