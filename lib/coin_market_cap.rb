require 'httparty'

class CoinMarketCap
    include HTTParty
    default_timeout 25
    base_uri 'https://pro-api.coinmarketcap.com/v1/cryptocurrency'

    def initialize(query: {})
        @options = { 
            query: query,
            headers: { 
                "Accepts"  => "application/json",
                "X-CMC_PRO_API_KEY" => ENV["CMC_PRO_API_KEY"]
            }
        }
    end

    def listings_latest
        begin
            request = self.class.get("/listings/latest", @options)
            request.parsed_response
        rescue => e
            Rails.logger.error "Rescued CoinMarketCap latest #{e.inspect}"
            return nil
        end
    end

    def quotes_latest
        begin
            request = self.class.get("/quotes/latest", @options)
            request.parsed_response
        rescue => e
            Rails.logger.error "Rescued CoinMarketCap latest #{e.inspect}"
            return nil
        end
    end

    def info
        begin
            request = self.class.get("/info", @options)
            request.parsed_response
        rescue => e
            Rails.logger.error "Rescued CoinMarketCap info #{e.inspect}"
            return nil
        end
    end
    
end
