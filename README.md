

# Crypto Portfolio Manager Pro

[crypto-portfolio-manager-pro.herokuapp.com](https://crypto-portfolio-manager-pro.herokuapp.com/)

Welcome to Crypto Portfolio Manager Pro. This is an app to manage your crypto currency portfolio. It calls the coinmarketcap api to get the latest Cryptocurrency data.

## Install

### Clone the repository

```shell
git clone gitlab.com/twinsfan421/spencers-crypto-portfolio-manager.git
```

### Check your Ruby version

```shell
ruby -v
```

The ouput should start with something like `ruby 2.6.5`

If not, I recommend using rbenv to install the right ruby version [rbenv](https://github.com/rbenv/rbenv) (it could take a while):

```shell
rbenv install 2.6.5
```
#### Rails version

Rails 6.0.2.1

### React

Rails 6 comes with Webpacker as the default JavaScript compiler and is configured with React

React 16.12.0

### Install dependencies

Using [Bundler](https://github.com/bundler/bundler) and [Yarn](https://github.com/yarnpkg/yarn):

```shell
bundle && yarn
```

### Set environment variables

To set up keys for your local instance variables, you'll need to create an `app_environment_variable.rb` file in the config directory. I've included `sample_app_environment_variable.rb` you can copy and fill in to create.


### Initialize the database

```shell
rails db:create db:migrate db:seed
```

## Serve

To run rails
```shell
rails s 
```

To run the webpack server
```shell
bin/webpack-dev-server

```


