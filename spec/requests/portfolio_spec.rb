require "rails_helper"

RSpec.describe "Portfolio", type: :request do
  include Devise::Test::IntegrationHelpers

  describe "GET /" do
    context "when not logged in" do
      it "renders welcome" do
        get "/"
        expect(response.body).to include "home_container"
      end
    end

    context "when logged in" do
      let(:user) { create(:user) }

      before do
        sign_in user
      end

      it "renders portfolio" do
        get "/"
        expect(response.body).to include "packs-test/js/portfolio"
      end
    end
  end

end