require 'rails_helper'

RSpec.describe PagesController, type: :controller do

    login_user
    
    let(:valid_attributes) {
        { }
    }

    let(:valid_session) { {} }

    describe "GET #welcome" do
        it "returns a success response" do
            #Dashboard.create! valid_attributes
            get :welcome, params: {}#, session: valid_session
            expect(response).to be_successful # be_successful expects a HTTP Status code of 200
            # expect(response).to have_http_status(302) # Expects a HTTP Status code of 302
        end
    end

    describe "GET #portfolio" do
        it "returns a success response" do
            get :portfolio, params: {}
            expect(response).to be_successful 
        end
    end
end