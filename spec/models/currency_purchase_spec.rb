require 'rails_helper'

RSpec.describe CurrencyPurchase, type: :model do
  it { should validate_presence_of(:purchase_price) }
  it { should validate_presence_of(:units) }
  it { should validate_numericality_of(:units).is_greater_than(0) }
  it { should validate_numericality_of(:purchase_price).is_greater_than(0).is_less_than(1000000) }

end
