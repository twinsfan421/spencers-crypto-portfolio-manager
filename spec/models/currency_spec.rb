require 'rails_helper'

RSpec.describe Currency, type: :model do

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:slug) }
  it { should validate_presence_of(:currency_symbol) }
  it { should validate_presence_of(:max_supply) }

  describe "valid Factory" do
    it "has a valid factory" do
      expect(build(:currency)).to be_valid
    end
  end

  context "valid coin market cap api" do
    let(:currency) { create(:currency) }

    before do
      file_content = file_fixture("coin_market_cap_latest.json").read
      json = JSON.parse(file_content, symbolize_names: true)
      stub_request(:any, 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?limit=5000').
      to_return(status: 200, body: json, headers: {"Content-Type"=> "application/json"})
    end

    it "returns correct current_price" do
      expect(currency.current_price).to eq(9492.68675522)
    end

    it "returns correct calculate_value" do
      expect(currency.calculate_value(2)).to eq(18985.37)
    end

    it "current price returns nil if no match" do
      fake_currency = create(:currency, name: "fake", slug: "fake")
      expect(fake_currency.current_price).to eq(nil)
    end
  end

  context "coin market cap api error" do
    let(:currency) { create(:currency) }

    before do
      stub_request(:any, 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?limit=5000').
      to_return(status: 404, body: "", headers: {"Content-Type"=> "application/json"})
    end

    it "current_price returns nil" do
      expect(currency.current_price).to eq(nil)
    end

    it "calculate_value returns nil" do
      expect(currency.calculate_value(2)).to eq(nil)
    end
  end
end
