FactoryBot.define do
  factory :currency do
    name { "Bitcoin" }
    description { Faker::ChuckNorris.fact }
    max_supply { Faker::Number.number(digits: 10) }
    currency_symbol { Faker::CryptoCoin.coin_hash }
    slug { "bitcoin" }
  end
end
