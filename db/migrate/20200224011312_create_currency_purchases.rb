class CreateCurrencyPurchases < ActiveRecord::Migration[6.0]
  def change
    create_table :currency_purchases do |t|
      t.integer :user_id
      t.integer :currency_id
      t.integer :units
      t.decimal :purchase_price, precision: 15, scale: 12
      t.date :purchase_date
      t.string :note
      t.timestamps
    end
    add_index("currency_purchases", ['user_id', 'currency_id'])
  end
end