class ChangePurchasePriceInCurrencyPurchases < ActiveRecord::Migration[6.0]
  def change
    change_column :currency_purchases, :purchase_price, :decimal, :precision => 15, :scale => 8
  end
end