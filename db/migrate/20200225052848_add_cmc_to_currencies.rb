class AddCmcToCurrencies < ActiveRecord::Migration[6.0]
  def change
    add_column :currencies, :cmc_id, :integer
  end
end
