class AddUsernameToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :username, :string
    change_column :users, :email, :string, :null => true
  end
end
